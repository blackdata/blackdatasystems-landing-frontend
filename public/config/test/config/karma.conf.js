/**
 *  NodeJS configuration used for the Karma Server
 *
 *  This code does three (3) things:
 *  1 ) configures NodeJS Karma server with frameworks: Jasmine and RequireJS
 *  2 ) auto-loads the jQuery, AngularJS, and AngularJS libs
 *  3 ) configures paths that should be included in the browser using <script> tag? or loaded them manually, eg. using Require.js.
 *
 */
module.exports = function (config)
{
    'use strict';

    config.set({

    // base path, that will be used to resolve files and exclude
    basePath: '../../',

    /**
     *  frameworks to use; by specifying `requirejs` we do not have to manually
     *  load them here in the config
     */
    frameworks: [ "jasmine", 'requirejs' ],


    // list of files / patterns to load in the Browser (using script tags)
    // @see http://karma-runner.github.io/0.8/config/files.html

    files: [

        // Load these files in this order...
        'bower_components/jquery/jquery.min.js',
        'bower_components/angular/angular.js',
        'bower_components/angular-mocks/angular-mocks.js',
        'bower_components/angular-ui-router/release/angular-ui-router.js',
        'bower_components/angular-foundation/mm-foundation.js',
        'bower_components/angular-foundation/mm-foundation-tpls.js',
        'bower_components/angular-local-storage/dist/angular-local-storage.js',
        'bower_components/angular-validation-match/dist/angular-validation-match.js',


        // Use `included = false` to let requireJS load them as needed
        // ... listed here so they can be resolved relative to the baseURL

        { pattern: 'src/**/*.js',                                        included: false },
        { pattern: 'test/spec/**/*Spec.js',                              included: false },
        { pattern: 'bower_components/zxcvbn/zxcvbn.js',                                    included: false },
        { pattern: 'src/libsodium_custom/dist/modules/libsodium.js',             included: false },
        { pattern: 'src/libsodium_custom/dist/modules/libsodium-wrappers.js',    included: false },
        { pattern: 'bower_components/compressjs/main.js',                                  included: false },
        //CompressJS Libs ////////////////////////////////////////////////////////////////////////
        { pattern: 'bower_components/compressjs/lib/freeze.js',                            included: false },
        { pattern: 'bower_components/compressjs/lib/BitStream.js',                         included: false },
        { pattern: 'bower_components/compressjs/lib/Stream.js',                            included: false },
        { pattern: 'bower_components/compressjs/lib/BTW.js',                               included: false },
        { pattern: 'bower_components/compressjs/lib/Context1Model.js',                     included: false },
        { pattern: 'bower_components/compressjs/lib/DefSumModel.js',                       included: false },
        { pattern: 'bower_components/compressjs/lib/FenwickModel.js',                      included: false },
        { pattern: 'bower_components/compressjs/lib/MTFModel.js',                          included: false },
        { pattern: 'bower_components/compressjs/lib/NoModel.js',                           included: false },
        { pattern: 'bower_components/compressjs/lib/Huffman.js',                           included: false },
        { pattern: 'bower_components/compressjs/lib/RangeCoder.js',                        included: false },
        { pattern: 'bower_components/compressjs/lib/BWTC.js',                              included: false },
        { pattern: 'bower_components/compressjs/lib/BWT.js',                               included: false },
        { pattern: 'bower_components/compressjs/lib/Bzip2.js',                             included: false },
        { pattern: 'bower_components/compressjs/lib/Dmc.js',                               included: false },
        { pattern: 'bower_components/compressjs/lib/Lzjb.js',                              included: false },
        { pattern: 'bower_components/compressjs/lib/LzjbR.js',                             included: false },
        { pattern: 'bower_components/compressjs/lib/Lzp3.js',                              included: false },
        { pattern: 'bower_components/compressjs/lib/PPM.js',                               included: false },
        { pattern: 'bower_components/compressjs/lib/Simple.js',                            included: false },
        { pattern: 'bower_components/compressjs/lib/Util.js',                              included: false },
        { pattern: 'bower_components/compressjs/lib/LogDistanceModel.js',                  included: false },
        { pattern: 'bower_components/compressjs/lib/CRC32.js',                             included: false },
        { pattern: 'bower_components/compressjs/lib/HuffmanAllocator.js',                  included: false },
        //////////////////////////////////////////////////////////////////////////////////////////
        { pattern: 'bower_components/requirejs-web-workers/src/worker.js',                 included: false },
        { pattern: 'bower_components/sql.js/js/worker.sql.js',                             included: false },


        'test/config/test-main.js'
    ],

    // list of files to exclude
    exclude: ['src/main.js'],


    // web server port
    port: 9876,


    // cli runner port
    runnerPort: 9100,

    reporters: [ 'progress', 'dots', 'html' ],

    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    // - process.env.TRAVIS
    browsers: ['Chrome' ],

    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000,

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,


    // List of karma plugins
    plugins: [
      'karma-requirejs',
      'karma-jasmine',
      'karma-jasmine-html-reporter',
      'karma-chrome-launcher'
      //'karma-firefox-launcher',
    ]
  });
};
