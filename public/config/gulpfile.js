/*
* gulp arguments:
*   --env   (dev/prod)
*   --host  ('127.0.0.0')
*   --bws   ('google chrome'/'firefox')
*
*   Example:
*       gulp [task] -env='prod' --host='127.0.0.1' --bws='firefox'
*/
// Fix for task _css to work, possible solution => update nodejs.
require('es6-promise').polyfill();

// Load plugins
var gulp           = require('gulp'),
    karma          = require('karma').server,
    del            = require('del'),
    browserSync    = require('browser-sync'),
    reload         = browserSync.reload,
    runSequence    = require('run-sequence'),
    fs             = require('fs'),
    argv           = require('yargs').argv,
    shell          = require('gulp-shell'),
    plugins        = require('gulp-load-plugins')(),
    uglify         = require('gulp-uglify'),
    jsonminify     = require('gulp-jsonminify'),
    imagemin       = require('gulp-imagemin'),
    cssnano        = require('gulp-cssnano'),
    pngquant       = require('imagemin-pngquant'),
    minifyHTML     = require('gulp-minify-html'),
    concat         = require('gulp-concat'),
    ngAnnotate     = require('gulp-ng-annotate'),
    stripDebug     = require('gulp-strip-debug'),

    serverBrowser  = (argv && argv.bws)?argv.bws:'google chrome',

    bases          = {
        host: (argv && argv.host)?argv.host:'http://landing.local',
        app: '../app/',
        assets: {
            images: '../app/assets/images/',
            css: '../app/assets/css/'
        },

        build: '../build/dist',
        karma: '/test/config/karma.conf.js'
    },
    buildDistFiles = [bases.app + '**', '!'+ bases.app +'assets/{styles,styles/**}'],
    browserSyncOptions = {
        watchTask: true,
        open: 'external',
        host: bases.host,
        proxy: bases.host,
        port: 3000,
        browser: serverBrowser // Open the site in Chrome, Firefox or together
    };

// TODO: Start the Browsersync Static Server + Watch files
gulp.task('_serve', function() {

    // Close the tab when Ctr+C in terminal
    browserSync.use({
        plugin: function () {},
        hooks: {
            'client:js': fs.readFileSync('./closer.js', 'utf-8')
        }
    });

    browserSync(browserSyncOptions);
});


/***********************************************
 * PAGES, SCRIPTS AND CSS
 ***********************************************/
// Task: pages
gulp.task('_pages', function() {
    return plugins.plumber()
        .pipe(gulp.src(bases.app + 'assets/views/**/*.html'))
        .pipe(plugins.htmlhint('.htmlhintrc'))
        .pipe(plugins.htmlhint.reporter())
        .pipe(plugins.size({showFiles: true, title: 'html'}))
        .pipe(plugins.changed(bases.app + 'assets/views/*.html'))
        .pipe(browserSync.stream({once: true}))
        .pipe(plugins.plumber.stop())
        .pipe(plugins.notify({message: 'Views ready', onLast: true}));

});

// Task: scripts
gulp.task('_scripts', function(){
    return plugins.plumber()
        .pipe(gulp.src(bases.app + 'src/**/*.js'))
        .pipe(plugins.size({showFiles: true, title: 'js'}))
        .pipe(plugins.changed(bases.app + 'src/**/*.js', {hasChanged: plugins.changed.compareSha1Digest}))
        .pipe(browserSync.stream({once: true}))
        .pipe(plugins.plumber.stop())
        .pipe(plugins.notify({message: 'Scripts ready', onLast: true}));
});

// Task: _jshint
gulp.task('_jshint', function() {
    return gulp.src(bases.app + 'src/**/*.js')
        .pipe(plugins.jshint('.jshintrc'))
        .pipe(plugins.jshint.reporter('jshint-stylish'));
});

// Task: css. Compile sass, filter the results, inject CSS into all browsers
gulp.task('_process-css', function () {
    return plugins.plumber()
        .pipe(plugins.rubySass(bases.app + 'assets/styles/**/*.scss', {
            stopOnError: true,
            cacheLocation: bases.app + '.sass-cache'
        }))
        .pipe(plugins.autoprefixer('last 10 versions'))
        .pipe(plugins.changed(bases.app + 'assets/styles/**/*.scss'))
        .pipe(browserSync.stream({once: true}))
        .pipe(plugins.plumber.stop())
        .pipe(gulp.dest(bases.app + 'assets/css/'))
        .pipe(plugins.notify({message: 'CSS ready', onLast: true}));

});




/***********************************************
 * JSDOC
 ***********************************************/
// Task: list-tasks
gulp.task('list-tasks', plugins.taskListing);

// Task: jsdoc-clean
gulp.task('_jsdoc-clean', function () {
    return del(['../docs/'], {force: true});
});

// Task: jsdoc
gulp.task('_jsdoc', plugins.shell.task([
    'node_modules/jsdoc/jsdoc.js ' +
    '-c node_modules/angular-jsdoc/common/conf.json ' +              // config file
    '-t node_modules/angular-jsdoc/angular-template ' +              // template file
    '-d ../docs ' +                                                  // output directory
    'JSDOC_README.md ' +                                             // to include JSDOC_README.md as index contents
    '-r ' + bases.app + 'src ' + bases.app + 'assets/js'     // source code directory
    //'-u tutorials'                                                 // tutorials directory
]));

gulp.task('jsdoc', ['_jsdoc-clean', '_jsdoc']);




/***********************************************
 * BUIL FOLDER
 ***********************************************/
// Task: copy-build
gulp.task('_build-copy-dist', ['_build-clean'], function() {
    return gulp.src(buildDistFiles, { base: bases.app })
        .pipe(gulp.dest(bases.build))
        .pipe(plugins.notify({message: 'Copy Build done!', onLast: true}));
});

// Task: dev-symlinks
gulp.task('_dev-symlinks', /*['_build-clean'],*/ plugins.shell.task([
    'bash bash/dev_symlinks.sh'
]));

// Task: Remove the previous versions
gulp.task('_build-clean', function(){
    return del([bases.build, bases.app + '.sass-cache'], {force:true});
});

// Task: prod_compiler
gulp.task('_prod_compiler', plugins.shell.task([
    'bash bash/deployment.sh'
]));




/***********************************************
 * MINIFY Tasks
 ***********************************************/
// Task: copy-build
gulp.task('_minify_js', function() {
    return gulp.src([bases.app + '**/module.js', bases.app + '**/*.js', '!' + bases.app + 'src/' + 'config-require.js'])
        //.pipe(concat('src/landing/main.js'))  //TODO
        .pipe(stripDebug()) // Remove debugger, alert, console
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest(bases.build))
        .pipe(plugins.notify({message: 'Copy Build done!', onLast: true}));
    });

gulp.task('_minify_html', function() {
    return gulp.src(bases.app + '**/**.html')
        .pipe(minifyHTML({ empty: true }))
        .pipe(gulp.dest(bases.build));
});

gulp.task('_minify_json', function () {
    return gulp.src(bases.app + 'assets/data/**/**.json')
        .pipe(jsonminify())
        .pipe(gulp.dest(bases.build + '/assets/data'));
});

gulp.task('_minify_img', function () {

    return gulp.src(bases.assets.images + '*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [
                {removeViewBox: false},
                {cleanupIDs: false}
            ],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(bases.build + '/assets/images/'));
});

gulp.task('_minify_css', function () {

    return gulp.src(bases.app + '**/**.css')
        .pipe(cssnano({discardComments: {removeAll: true}}))
        .pipe(gulp.dest(bases.build));
});



/***********************************************
 * PUBLIC TASKS
 ***********************************************/
// Tast: watch
gulp.task('watch', function () {
    gulp.watch(bases.app + 'assets/styles/**/*.scss', ['_process-css']);
    gulp.watch(bases.app + 'assets/views/*.html', ['_pages']);
    gulp.watch(bases.app + 'src/**/*.js', ['_scripts']);
    gulp.watch(bases.app + 'index.html').on('change', reload);
});

gulp.task('test', function(done){
    karma.start({
        configFile: __dirname + bases.karma,
        singleRun: true
    }, done);
});

gulp.task('prod', function(){
    runSequence(
        '_build-copy-dist',
        '_process-css',
        [
            '_minify_js',
            '_minify_img',
            //'_minify_css',
            '_minify_json',
            '_minify_html'
        ],
        '_prod_compiler'
    );
});

gulp.task('dev', function() {
    return runSequence(
        '_build-clean',
        '_dev-symlinks',
        '_jshint',
        ['_pages',
        '_process-css',
        '_scripts']
    );
});




// Deprecated. TODO: modernize
gulp.task('help', ['list-tasks'], function() {
    var helpMsg = 'gulp arguments:\n\n\t--env\t(dev/prod)\n\n\t--host\t(\'127.0.0.0\')\n\n\t--bws\t(\'google chrome\'/\'firefox\')\n\n'+
        '\tExample:\n\tgulp [task] -env=\'prod\' --host=\'127.0.0.1\' --bws=\'firefox\'';
    console.log('----------------------------------------\n\n' + helpMsg + '\n\n----------------------------------------');
});