#!/bin/bash

DATE=`date +%Y.%m.%d-%H.%M.%S`
DEPLOY_DIR=../../deployment/
OUTPUT_DIR=$DEPLOY_DIR$DATE/

FILES=( "application" "public/build" "system" "index.php" ".htaccess" )

mkdir $DEPLOY_DIR
mkdir $OUTPUT_DIR
mkdir ${OUTPUT_DIR}public

for i in "${FILES[@]}"
do
    cp -r ../../$i ${OUTPUT_DIR}${i}
done