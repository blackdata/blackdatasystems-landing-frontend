#!/usr/bin/env bash

## Symbolic links of: css, i18n, partials, scripts, vendors
mkdir -p ../build/dist
cd ../build/dist
ln -s ../../app/* ./
cd -