if (typeof define !== 'function') {
    // to be able to require file from node
    var define = require('amdefine')(module);
}

define({
    baseUrl: 'public/build/',
    paths: {
        // Configure require plugins
        'domReady'                      : 'bower_components/requirejs-domready/domReady',

        // Configure libraries

        'jquery'                        : 'bower_components/jquery/dist/jquery.min',
        'ngBootstrap'                   : 'bower_components/angular-bootstrap/ui-bootstrap.min',
        'ngBootstrapTpls'               : 'bower_components/angular-bootstrap/ui-bootstrap-tpls.min',
        'angular'                       : 'bower_components/angular/angular.min',
        'underscore'                    : 'bower_components/underscore/underscore.min',
        'ngSanitize'                    : 'bower_components/angular-sanitize/angular-sanitize.min',
        'ngMessages'                    : 'bower_components/angular-messages/angular-messages.min',
        'ui.router'                     : 'bower_components/angular-ui-router/release/angular-ui-router.min',
        'ngAnimate'                     : 'bower_components/angular-animate/angular-animate.min',
        'angularTranslate'              : 'bower_components/angular-translate/angular-translate.min',
        'angularTranslateStaticFiles'   : 'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min',
        'ngLocalStorage'                : 'bower_components/angular-local-storage/dist/angular-local-storage.min',
        'jsSHA'                         : 'bower_components/jsSHA/src/sha',
        'Modernizr'                     : 'dist/assets/libs/js/uiMorph/modernizr.custom',
        'uiMorph'                       : 'dist/assets/libs/js/uiMorph/uiMorphingButton_fixed',
        'slick-carousel'                : 'bower_components/slick-carousel/slick/slick.min',
        'angular-slick'                 : 'bower_components/angular-slick/dist/slick.min',

        // Configure alias to full paths
        'landing'                       : 'dist/src/landing',
        'core'                          : 'dist/src/core'
    },

    shim: {
        'jquery'                        : { exports: '$' },
        'angular'                       : { exports: 'angular', deps: ['jquery'] },
        'Modernizr'                     : { deps: ['angular'] },
        'ngBootstrap'                   : { deps: ['jquery', 'angular'] },
        'ngBootstrapTpls'               : { deps: ['ngBootstrap', 'angular'] },
        'ngMessages'                    : { deps: ['angular'] },
        'ngSanitize'                    : { deps: ['angular'] },
        'ngFlash'                       : { deps: ['angular'] },
        'ngAnimate'                     : { deps: ['angular'] },
        'ngResource'                    : { deps: ['angular'] },
        'ui.router'                     : { deps: ['angular'] },
        'ngLocalStorage'                : { deps: ['angular'] },
        'angularTranslate'              : { deps: ['angular'] },
        'angularTranslateStaticFiles'   : { deps: ['angular', 'angularTranslate'] },
        'uiMorph'                       : { deps: ['angular', 'Modernizr'] },
        'angular-slick'                 : { deps: ['angular'] },
        'slick-carousel'                : { deps: ['angular-slick'] }
    },

    // Prevent 'Load timeout for modules'
    waitSeconds: 0

    //// Set cache beater
    //// IMPORTANT!! Remove it for production environments!!
    //urlArgs: 'bust=v0.4.0'
});
