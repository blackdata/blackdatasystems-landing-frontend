(function (define) {
    'use strict';

    define([], function ()
    {
        /**
         *
         * @param $http
         * @param localStorageService
         * @returns {{setAuthorizationSession: SessionConstantsService.setAuthorizationSession, isSessionActive: SessionConstantsService.isSessionActive, setLanguage: SessionConstantsService.setLanguage, getLanguage: SessionConstantsService.getLanguage, setIsCaptchaDone: SessionConstantsService.setIsCaptchaDone, isCaptchaDone: SessionConstantsService.isCaptchaDone, clearCurrentSession: SessionConstantsService.clearCurrentSession}}
         * @constructor
         */
        var SessionConstantsService = function ($http, localStorageService)
        {
            /**
             * - Saves the authorization session into sessionStorage.
             * - Puts the authorization session in the future requests header.
             * @param session
             */
            function setAuthorizationSession(session) {
                localStorageService.set('SESSION', session);
                $http.defaults.headers.common.Authorization = session;
            }

            /**
             * - Sets the authorizationSession.
             * - Returns if there is an active session.
             * @returns {*|boolean}
             */
            function isSessionActive() {
                var sessionStorageSession = localStorageService.get('SESSION');
                setAuthorizationSession(sessionStorageSession);
                return (sessionStorageSession && sessionStorageSession !== '');
            }

            /**
             * Saves the language key.
             * @param language
             */
            function setLanguage(language) {
                localStorageService.set('LANG', language);
            }

            /**
             * Gets the current language.
             * @returns {*}
             */
            function getLanguage() {
                return localStorageService.get('LANG');
            }

            /**
             * Saves the is captcha done flag.
             * @param isCaptchaDone
             */
            function setIsCaptchaDone(isCaptchaDone) {
                localStorageService.set('IS_CAPTCHA_DONE', isCaptchaDone);
            }

            /**
             * True if captcha challenge is already solved.
             * @returns {*}
             */
            function isCaptchaDone() {
                return localStorageService.get('IS_CAPTCHA_DONE');
            }

            /**
             * Destroys the session storage current session.
             */
            function clearCurrentSession() {
                localStorageService.remove('SESSION');
            }

            return {
                setAuthorizationSession: setAuthorizationSession,
                isSessionActive: isSessionActive,
                setLanguage: setLanguage,
                getLanguage: getLanguage,
                setIsCaptchaDone: setIsCaptchaDone,
                isCaptchaDone: isCaptchaDone,
                clearCurrentSession: clearCurrentSession
            };
        };

        return ['$http', 'localStorageService', SessionConstantsService];
    });
}(define));