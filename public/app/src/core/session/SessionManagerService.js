(function (define) {
    'use strict';

    define(['jsSHA'], function (jsSHA)
    {
        /**
         *
         * @param $timeout
         * @param $q
         * @param connectionService
         * @param localStorageService
         * @param sessionConstantsService
         * @returns {{initSession: SessionManagerService.initSession}}
         * @constructor
         */
        var SessionManagerService = function ($timeout, $q, connectionService, localStorageService, sessionConstantsService)
        {
            /**
             * The possible character where extract the challenge solution.
             * @type {string}
             */
            var challengePattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.';

            /**
             * Max of loops to prevent an infinite loop.
             * @type {number}
             */
            var maxChallengeLoops = 3;

            /**
             * Challenge data retrieved from server and used to resolve the session challenge.
             * @type {{}}
             */
            var challenge = {};

            /**
             * Session tries timer.
             * @type {null}
             */
            var triesTimeout = null;

            /**
             * Number of init session tries done.
             * @type {number}
             */
            var currentSessionTries = 0;

            /**
             * Maximum number of tries until sleeping process start.
             * @type {number}
             */
            var maxSessionTries = 5;

            /**
             * Time to sleep the init session retries.
             * @type {number}
             */
            var retryMinutes = 1;

            /**
             * Generates a new SHA-256 from the join of: the signature and the chars found.
             * @param chars
             * @returns {*}
             * @private
             */
            function _generateChallengeString(chars) {
                var x = [challenge.signature, chars].join(''),
                    shaObj = new jsSHA('SHA-256', 'TEXT'),
                    hash;

                shaObj.update(x);
                hash = shaObj.getHash('HEX');
                return hash;
            }

            /**
             * Validates if the number of 'str' leading zeros match with the 'zeros' given.
             * @param str
             * @param zeros
             * @returns {boolean}
             * @private
             */
            function _leadingZerosValidation(str, zeros) {

                var zeroCounter = 0;
                for(var i = 0; i < str.length; i++) {
                    if(parseInt(str[i], 10) !== 0) {
                        break;
                    } else {
                        zeroCounter++;
                    }
                }
                return zeros === zeroCounter;
            }

            /**
             * - Generates a SHA256 from chars given.
             * - Validates if the result pass the _leadingZerosValidation
             * @param chars
             * @returns {*}
             * @private
             */
            function _resolveChallenge(chars) {

                var result = _generateChallengeString(chars);

                // Returns the characters if OK, otherwise False.
                if(_leadingZerosValidation(result, challenge.zeros)) {
                    return chars;
                } else {
                    return false;
                }
            }

            /**
             *
             * @param charset
             * @param level
             * @param prefix
             * @returns {string|String}
             * @private
             */
            function _childLoop(charset, level, prefix) {
                var status = null;
                for(var x = 0; x < charset.length; x++){
                    if( parseInt(level, 10) !== 0) {
                        status = _childLoop(charset, level - 1, prefix + charset[x]);
                    } else {
                        status = _resolveChallenge(prefix + charset[x]);
                    }
                    if (typeof status === 'string' || status instanceof String) {
                        return status;
                    }
                }
            }

            /**
             *
             * @param charset
             * @returns {*}
             * @private
             */
            function _parentLoop(charset) {

                var currentLevel = 0,
                    prefix = '',
                    status = _resolveChallenge(prefix);

                if(status !== false) {
                    return status;
                }

                while (true) {
                    if(currentLevel === maxChallengeLoops) {
                        return false;
                    }

                    for(var x = 0; x < charset.length; x++){
                        if(parseInt(currentLevel, 10) !== 0) {
                            status = _childLoop(charset, currentLevel -1, prefix + charset[x]);
                        } else {
                            status = _resolveChallenge(charset[x]);
                        }
                        if (typeof status === 'string' || status instanceof String) {
                            return status;
                        }
                    }
                    currentLevel++;
                }
            }

            /**
             * - Resolves the challenge and send the solution.
             * - If solution is invalid, restarts the initSession process for 5 times. And sleeps for 1 minute.<br>
             * - Alter that, retries 5 more times.
             * @param deferred
             * @param _challenge
             * @private
             */
            function _doChallenge(deferred, _challenge) {

                challenge = _challenge;
                var result = _parentLoop(challengePattern);

                if(result === false) {
                    console.warn('Something went wrong with the session hash generation.');
                } else {
                    challenge.solution = result;
                    connectionService.sessions.resolveChallenge(challenge).then(function (r) {
                        console.log('setSession response', r);

                        sessionConstantsService.setAuthorizationSession(r.response.session);
                        clearSessionTimer();
                        deferred.resolve('Session challenge solved!');
                    }, function (r) {

                        // Infinite loop. 5 tries of initSession and wait for 1 minute.
                        currentSessionTries++;
                        if(currentSessionTries < maxSessionTries) {
                            return _getNewSessionChallenge(deferred);
                        } else {
                            triesTimeout = $timeout(function () {
                                clearSessionTimer();
                                return _getNewSessionChallenge(deferred);
                            }, 1000 * 60 * retryMinutes);
                        }
                        //deferred.resolve({message: ['Error resolving challenge: ', r.message].join('')});
                    });
                }
            }

            /**
             * Initializes the session challenge by getting a new session challenge.
             * @param deferred
             * @private
             */
            function _getNewSessionChallenge(deferred) {
                connectionService.sessions.get().then(function (r) {
                    _doChallenge(deferred, r.response);
                }, function (r) {
                    deferred.resolve({message: ['Error getting the session challenge: ', r.message].join('')});
                });
            }

            /**
             * @memberOf SessionManagerService
             * @method clearSessionTimer
             * @desc
             *  Resets the timeout retries information.
             */
            function clearSessionTimer() {
                $timeout.cancel(triesTimeout);
                currentSessionTries = 0;
            }

            /**
             * - Checks if there is an existing active session. I true, allows the user to enter to the app.
             * - If not, calls the _getNewSessionChallenge method.
             * @returns {*}
             */
            function initSession() {

                var deferred = $q.defer();

                clearSessionTimer();

                if(sessionConstantsService.isSessionActive()) {
                    deferred.resolve('Session retrieved!');
                } else {
                    _getNewSessionChallenge(deferred);
                }
                return deferred.promise;
            }

            return {
                initSession: initSession
            };
        };

        return ['$timeout', '$q', 'connectionService', 'localStorageService', 'sessionConstantsService', SessionManagerService];
    });
}(define));