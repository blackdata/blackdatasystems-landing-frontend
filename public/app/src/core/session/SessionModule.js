(function (define) {
    'use strict';

    define([
            'angular',
            'core/session/SessionManagerService',
            'core/session/SessionConstantsService'
        ],
        /**
         * 
         * @param angular
         * @param SessionManagerService
         * @param SessionConstantsService
         * @returns {string}
         */
        function ( angular, SessionManagerService, SessionConstantsService )
        {
            var moduleName = 'blackdataLanding.SessionModule';

            angular
                .module(moduleName, [])
                .service('sessionManagerService', SessionManagerService)
                .service('sessionConstantsService', SessionConstantsService);

            return moduleName;
        });
}(define));