(function (define) {
    'use strict';

    define([
            'angular',
            'core/captcha/CaptchaModule',
            'core/connection/ConnectionModule',
            'core/session/SessionModule'
        ],
        /**
         *
         * @param angular
         * @param CaptchaModule
         * @param ConnectionModule
         * @param SessionModule
         * @returns {string}
         */
        function ( angular, CaptchaModule, ConnectionModule, SessionModule )
        {
            var moduleName = 'blackdataLanding.CoreModule';
            angular
                .module(moduleName, [CaptchaModule, ConnectionModule, SessionModule])
                .service('utilsService', function () {

                    var Arrays = {
                        lastFrom: function (array) {
                            return array[array.length - 1];
                        },
                        getItemById: function (elements, key, value) {
                            for(var i = 0; i < elements.length; i ++) {
                                if(elements[i][key] === value) {
                                    return elements[i];
                                }
                            }
                        },
                        isArray: function (obj) {
                            return obj instanceof Array;
                        },
                        contains: function (source, target) {
                            try {
                                return source.indexOf(target) !== -1;
                            } catch(e) {
                                throw Error(e);
                            }
                        }
                    };

                    var Objects = {
                        isEmpty: function (obj) {
                            for(var prop in obj) {
                                if(obj.hasOwnProperty(prop))return false;
                            }
                            return true;
                        },
                        copy: function (object) {
                            return angular.copy(object);
                        }
                    };

                    return {
                        Arrays: Arrays,
                        Objects: Objects
                    };
                })
                .service( 'focusService', ['$timeout', '$window', function ($timeout, $window)
                {
                    return {
                        focusElementById: function (id) {
                            $timeout(function () {
                                var element = $window.document.getElementById(id);
                                if(element) {
                                    element.focus();
                                } else {
                                    console.warn('Focus element not found');
                                }
                            });
                        }
                    };
                }])
                .filter('dateFormat', function () {
                    return function (input) {
                        if(!input) return '';
                        input = new Date(input);
                        var res = (input.getMonth()+1) + '/' + input.getDate() + '/' + input.getFullYear() + ' ';
                        var hour = input.getHours();
                        var ampm = 'AM';
                        if(hour === 12) ampm = 'PM';
                        if(hour > 12){
                            hour-=12;
                            ampm = 'PM';
                        }
                        var minute = input.getMinutes()+1;
                        if(minute < 10) minute = '0' + minute;
                        res += hour + ':' + minute + ' ' + ampm;
                        return res;
                    };
                })
                .directive('autoFocus', function ($timeout) {
                    return {
                        restrict: 'AC',
                        link: function (_scope, _element) {
                            $timeout(function (){
                                _element[0].focus();
                            }, 500);
                        }
                    };
                });
            return moduleName;
        });
}(define));