﻿(function (define) {
    'use strict';

    define([], function ()
    {
        /**
         *
         * @param $location
         * @param $rootScope
         * @param $timeout
         * @param $q
         * @param $http
         * @param utilsService
         * @param sessionConstantsService
         * @param $injector
         * @returns {{sessions: {apiCallUrl: string, errorCodes: {post: {400: string[]}}, get: sessions.get, resolveChallenge: sessions.resolveChallenge}, captchas: {apiCallUrl: string, silentSession: string[], errorCodes: {post: {400: string[], 401: string[]}}, get: captchas.get, resolveChallenge: captchas.resolveChallenge}, contact: {apiCallUrl: string, silentSession: string[], errorCodes: {post: {400: string[], 401: string[]}}, create: contact.create}}}
         * @constructor
         */
        var ConnectionService = function ($location, $rootScope, $timeout, $q, $http, utilsService, sessionConstantsService, $injector)
        {
            // TODO: refactoritzar la circular dependency (sessionService i connectionService). Deixar d'utilitzar el injector?
            var sessionManagerService = null;
            $timeout(function () {
                sessionManagerService = $injector.get('sessionManagerService');
            });

            /**
             * Prepares the request to be executed in _apiCall.
             * @param action
             * @param apiCallUrl
             * @param source
             * @param dataToSend
             * @param urlParams
             * @returns {*}
             * @private
             */
            function _prepareApiCall(action, apiCallUrl, source, dataToSend, urlParams) {

                var url = utilsService.Objects.copy(apiCallUrl);
                for(var key in urlParams) {
                    if(urlParams.hasOwnProperty(key)) {
                        url += ['?', key, '=', urlParams[key]].join('');
                    }
                }

                var deferred = $q.defer();
                _apiCall(deferred, action, url, source, dataToSend);
                return deferred.promise;
            }

            /**
             * Executes the request.
             * @param deferred
             * @param action
             * @param apiCallUrl
             * @param source
             * @param dataToSend
             * @private
             */
            function _apiCall(deferred, action, apiCallUrl, source, dataToSend) {

                $http[action](apiCallUrl, dataToSend)
                    .success(function (data, status, headers, config) {
                        deferred.resolve({response: data});
                    })
                    .error(function (data, status, headers, config) {
                        if(_isErrorInApiCall(source, data, action)) {
                            deferred.reject({response: data, status: status});
                        } else {
                            _globalErrorBehavior(deferred, action, source, data, function () {
                                return _apiCall(deferred, action, apiCallUrl, source, dataToSend);
                            });
                        }
                    });
            }

            /**
             * Treats the global error response.
             * @param deferred
             * @param action
             * @param source
             * @param data
             * @param recallCallback
             * @private
             */
            function _globalErrorBehavior(deferred, action, source, data, recallCallback) {

                switch(data.http_code) {
                    case 400:
                    case 401:
                        if((data.error_code === '001' || data.error_code === '003') && _mustDoSilentSession(source, action)) { // Session does not exist

                            console.warn('Silent init session.');

                            if(sessionManagerService === null)sessionManagerService = $injector.get('sessionManagerService');

                            sessionConstantsService.clearCurrentSession();
                            sessionManagerService.initSession()
                                .then(function () {
                                    console.info('Init silent session OK');
                                    recallCallback(data);
                                }, function () {
                                    console.info('Init silent session ERROR');
                                    deferred.reject({message: 'Init silent session ERROR'});
                                });
                        } else {
                            deferred.reject(data);
                        }
                        break;
                    default:
                        // Default as 500 http_code
                        console.info('Application error.');
                        deferred.reject(data);
                        break;
                }
            }

            /**
             * Search if http error code retrieved is one of the source available errors.
             * @param source
             * @param action
             * @returns {boolean}
             * @private
             */
            function _mustDoSilentSession(source, action) {
                if(source.silentSession && source.silentSession !== null) {
                    return ($.inArray(action, source.silentSession) !== -1);
                } else {
                    return false;
                }
            }

            /**
             * Search if http error code retrieved is one of the source available errors.
             * @param source
             * @param data
             * @param action
             * @returns {boolean}
             * @private
             */
            function _isErrorInApiCall(source, data, action) {

                if (!source.errorCodes || !source.errorCodes[action] || source.errorCodes[action] === null) {
                    return false;
                }
                return ($.inArray(data.error_code, source.errorCodes[action][data.http_code]) !== -1);
            }

            return {
                sessions: {
                    apiCallUrl: 'session',
                    errorCodes: {
                        'post': {
                            400: ['003']
                        }
                    },
                    get: function () {
                        return _prepareApiCall('get', this.apiCallUrl, this);
                    },
                    resolveChallenge: function (data) {
                        return _prepareApiCall('post', this.apiCallUrl, this, data);
                    }
                },

                captchas: {
                    apiCallUrl: 'captcha',
                    silentSession: ['get', 'post'],
                    errorCodes: {
                        'post': {
                            400: ['002'],
                            401: ['004']
                        }
                    },
                    get: function () {
                        return _prepareApiCall('get', this.apiCallUrl, this);
                    },
                    resolveChallenge: function (data) {
                        return _prepareApiCall('post', this.apiCallUrl, this, data);
                    }
                },

                contact: {
                    apiCallUrl: 'contact',
                    silentSession: ['post'],
                    errorCodes: {
                        'post': {
                            400: ['002'],
                            401: ['005']
                        }
                    },
                    create: function (data) {
                        return _prepareApiCall('post', this.apiCallUrl, this, data);
                    }
                }
            };
        };

        return ['$location', '$rootScope', '$timeout', '$q', '$http', 'utilsService', 'sessionConstantsService', '$injector', ConnectionService];
    });
}(define));
