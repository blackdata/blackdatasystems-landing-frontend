(function (define) {
    'use strict';

    define([
            'angular',
            'core/connection/ConnectionService'
        ],
        /**
         * 
         * @param angular
         * @param ConnectionService
         * @returns {string}
         */
        function ( angular, ConnectionService )
        {
            var moduleName = 'blackdataLanding.ConnectionModule';

            angular
                .module(moduleName, [])
                .service('connectionService', ConnectionService);

            return moduleName;
        });
}(define));

