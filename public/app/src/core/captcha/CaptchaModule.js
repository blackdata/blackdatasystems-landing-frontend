(function (define) {
    'use strict';

    define([
            'angular',
            'core/captcha/CaptchaController',
            'core/captcha/CaptchaService'
        ],
        /**
         *
         * @param angular
         * @param CaptchaController
         * @param CaptchaService
         * @returns {string}
         */
        function ( angular, CaptchaController, CaptchaService )
        {
            var moduleName = 'blackdataLanding.CaptchaModule';

            angular
                .module(moduleName, [])
                .controller('CaptchaController', CaptchaController)
                .service('captchaService', CaptchaService);

            return moduleName;
        });
}(define));