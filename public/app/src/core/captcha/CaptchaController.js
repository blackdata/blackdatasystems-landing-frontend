(function (define) {
    'use strict';

    /**
     * Register the CaptchaController class with RequireJS
     */
    define( [], function ()
    {
        /**
         * Constructor function used by AngularJS to create instances of
         * a service, factory, or controller.
         *
         * @constructor
         */
        var CaptchaController = function ( $state, $scope, $rootScope, captchaService, focusService )
        {
            function demandCaptcha() {

                focusService.focusElementById('solution');

                captchaService.getCaptcha()
                    .then(function (r) {
                        $scope.captchaImage = ['data:image/gif;base64,', r.response.solution].join('');
                        $scope.captchaLoading = false;
                        $scope.solution = '';
                    }, function (r) {
                        console.info('Get captcha error', r);
                    });
            }

            function sendCaptchaSolution() {

                $scope.sendTried = true;
                $scope.captchaLoading = true;
                var solutionTreated = $scope.solution.toUpperCase();
                captchaService.sendSolution(solutionTreated)
                    .then(function (r) {
                        console.log(r);
                        $scope.captchaLoading = false;
                        console.log('SEND captcha OK!');

                        $rootScope.$broadcast('captcha_solved');

                    }, function (r) {
                        $scope.captchaLoading = false;

                        console.info('Send captcha error', r);

                        if(r.response && r.response.error_code) {
                            switch(r.response.error_code) {
                                case '002':
                                case '004':
                                    console.warn('SEND captcha error');
                                    $scope.solution = '';

                                    $scope.captchaForm.solution.$setValidity('invalidSolution', false);
                                    demandCaptcha();
                                    break;
                                default:
                                    console.warn('Send captcha error');
                                    break;
                            }
                        } else {
                            console.warn('Send captcha error');
                        }
                    });
            }

            $scope.onSendCaptchaSolution = function () {
                return sendCaptchaSolution();
            };

            $scope.onDemandCaptcha = function () {
                return demandCaptcha();
            };

            $scope.sendTried = false;
            $scope.captchaLoading = false;
            //$scope.captchaForm = null;
            $scope.solution = '';

            demandCaptcha();
        };

        return [ '$state', '$scope', '$rootScope', 'captchaService', 'focusService', CaptchaController];
    });
}(define));
