(function (define) {
    'use strict';

    define([], function () {

        /**
         *
         * @param $q
         * @param connectionService
         * @returns {{getCaptcha: getCaptcha, sendSolution: sendSolution}}
         * @constructor
         */
        var CaptchaService = function ($q, connectionService)
        {
            /**
             * Sends the captcha challenge solution.
             * @param solution
             * @returns {*}
             */
            function sendSolution(solution) {

                var deferred = $q.defer();
                var challenge = {
                    solution: solution
                };
                connectionService.captchas.resolveChallenge(challenge)
                    .then(function (r) {
                        console.log('sendCaptcha OK response', r);
                        deferred.resolve({response: r.response});
                    }, function (r) {
                        console.log('sendCaptcha ERROR response', r);
                        deferred.reject({response: r.response, message: ['Error sending the captcha solution: ', r.message].join('')});
                    });

                return deferred.promise;
            }

            /**
             * Gets the new captcha challenge.
             * @returns {*}
             */
            function getCaptcha() {

                var deferred = $q.defer();
                connectionService.captchas.get()
                    .then(function (r) {
                        console.log('getCaptcha OK response', r);
                        deferred.resolve({response: r.response});
                    }, function (r) {
                        console.log('getCaptcha ERROR response', r);
                        deferred.reject({response: r.response, message: ['Error getting the captcha image: ', r.message].join('')});
                    });
                return deferred.promise;
            }

            return {
                getCaptcha: getCaptcha,
                sendSolution: sendSolution
            };
        };

        return ['$q', 'connectionService', CaptchaService];
    });
}(define));