(function (define) {
    'use strict';

    define([], function () {

        /**
         *
         * @param $q
         * @param connectionService
         * @returns {{sendContactInformation: sendContactInformation}}
         * @constructor
         */
        var LandingService = function ($q, connectionService) {

            function sendContactInformation(contact) {

                var deferred = $q.defer();

                connectionService.contact.create(contact)
                    .then(function(r) {
                        deferred.resolve(r);
                    }, function (r) {
                        deferred.reject(r);
                    });

                return deferred.promise;
            }

            return {
                sendContactInformation: sendContactInformation
            };
        };

        return ['$q', 'connectionService', LandingService];
    });
}(define));