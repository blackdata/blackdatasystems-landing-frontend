(function (define) {
    'use strict';

    define([], function () {

        /**
         *
         * @param $timeout
         * @param $window
         * @returns {{focusElementById: focusElementById}}
         * @constructor
         */
        var FocusDirective = function ($timeout, $window)
        {
            return {
                focusElementById: function (id) {
                    $timeout(function () {
                        var element = $window.document.getElementById(id);
                        if(element) {
                            element.focus();
                        } else {
                            console.warn('Focus element not found');
                        }
                    });
                }
            };
        };

        return ['$timeout', '$window', FocusDirective];
    });
}(define));