(function (define) {
    'use strict';

    define( [], function ()
    {
        /**
         *
         * @param $rootScope
         * @param $scope
         * @param $timeout
         * @param landingService
         * @param sessionManagerService
         * @param focusService
         * @constructor
         */
        var LandingController = function ( $rootScope, $scope, $timeout, landingService, sessionManagerService, focusService )
        {
            function mobileCheck() {
                var check = false;
                (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true;})(navigator.userAgent||navigator.vendor||window.opera);
                return check;
            }
            $scope.isMobile = mobileCheck();

            $scope.contactFormSent = false;
            $scope.contactForm = {form: null};

            $scope.formTried = false;
            $scope.captchaRequired = false;
            $scope.contactInfo = {
                name: null,
                email: null,
                company: null,
                position: null,
                message: null
            };
            $scope.slickConfig = {
                method: {}
            };
            $scope.slickContentReady = false;

            $scope.sendingContact = false;

            // Scroll effect.
            $scope.goToElement = function (eID) {
                var target = $('#' + eID);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 500);
                    return false;
                }
            };

            $scope.goToTop = function () {
                $('html, body').animate({scrollTop: 0}, 500);
            };

            $scope.$on('captcha_solved', function () {
                $scope.captchaRequired = false;
            });

            $scope.onSendContactInformation = function () {

                console.log($scope.contactInfo, $scope.contactForm.form);

                $scope.sendingContact = true;

                landingService.sendContactInformation($scope.contactInfo)
                    .then(function (r) {
                        console.log(r);
                        $scope.contactInfo = {
                            name: null,
                            email: null,
                            company: null,
                            position: null,
                            message: null
                        };
                        $scope.sendingContact = false;
                        $scope.contactFormSent = true;
                        $scope.contactForm.form.$setPristine();
                    }, function (r) {
                        console.warn(r);
                        $scope.sendingContact = false;
                        if(r.response && r.response.error_code) {
                            if(r.response && r.response.error_code) {
                                switch(r.response.error_code) {
                                    case '002':
                                        if(r.response.field === 'email') {
                                            $scope.contactForm.form.email.$setValidity('invalidEmail', false);
                                            focusService.focusElementById('email');
                                        } else if(r.response.field === 'name') {
                                            $scope.contactForm.form.name.$setValidity('invalidName', false);
                                            focusService.focusElementById('name');
                                        } else if(r.response.field === 'company') {
                                            $scope.contactForm.form.company.$setValidity('invalidCompany', false);
                                            focusService.focusElementById('company');
                                        } else if(r.response.field === 'position') {
                                            $scope.contactForm.form.position.$setValidity('invalidPosition', false);
                                            focusService.focusElementById('position');
                                        } else if(r.response.field === 'message') {
                                            $scope.contactForm.form.message.$setValidity('invalidMessage', false);
                                            focusService.focusElementById('message');
                                        }
                                        break;
                                    case '005':
                                        $scope.captchaRequired = true;
                                        break;
                                    default:
                                        console.warn(r.message);
                                        break;
                                }
                            }
                        }
                    });
            };

            $scope.currentAreaID = '';

            $scope.showArea = function (eID) {

                $scope.currentAreaID = eID;
                $scope.slickContentReady = false;

                $('#landingSection2Areas').children('span').each(function (index, e) {
                    if(parseInt(eID.charAt(eID.length - 1), 10) === index + 1) {
                        $(e).addClass('item-selected');
                    } else {
                        $(e).removeClass('item-selected');
                    }
                });

                $scope.areaSelected = parseInt(eID, 10) - 1;
                $timeout(function () {
                    $scope.slickContentReady = true;
                }, 500);
            };

            // TODO: on change language, translate it!

            function _translateSlickContent() {
                $scope.areaContents = [];
                for(var j = 1; j <= 3; j++) {
                    $scope.areaContents.push([]);
                    for (var i = 1; i <= 3; i++) {
                        var areaZeroString = (j < 10)?'0':'',
                            itemZeroString = (j < 10)?'0':'';

                        $scope.areaContents[j - 1].push(
                            {
                                title: $rootScope.languagePreference.translateKey('LANDING.SECTION02.SLICK_CONTENT.AREA' + areaZeroString + j + '.TITLE' + itemZeroString + i),
                                text: $rootScope.languagePreference.translateKey('LANDING.SECTION02.SLICK_CONTENT.AREA' + areaZeroString + j + '.TEXT' + itemZeroString + i)
                            }
                        );
                    }
                }
                $scope.slickContentReady = true;
            }

            $rootScope.$on('CHANGE_LANGUAGE', function () {
                $timeout(function () {
                    _translateSlickContent();
                    $scope.showArea($scope.currentAreaID);
                }, 500);
            });

            (function init() {
                $scope.sessionReady = false;
                sessionManagerService.initSession()
                    .then(function (r) {
                        $scope.sessionReady = true;
                        console.log('Init session done!', r);
                        _translateSlickContent();
                        $scope.goToTop();
                        $scope.currentAreaID = '02';
                        $scope.showArea('02');
                    }, function (r) {
                        console.warn(r);
                    });
            })();
        };

        return [ '$rootScope', '$scope', '$timeout', 'landingService', 'sessionManagerService', 'focusService', LandingController];
    });
}(define));
