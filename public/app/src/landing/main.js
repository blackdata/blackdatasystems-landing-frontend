(function (define) {
    'use strict';

    define([
            'require',
            'angular',

            'jquery',
            'ngBootstrap',
            'ngBootstrapTpls',
            'ngAnimate',
            'angularTranslate',
            'angularTranslateStaticFiles',
            'ui.router',
            'ngSanitize',
            'ngMessages',
            'ngLocalStorage',
            'uiMorph',
            'Modernizr',
            'angular-slick',
            'slick-carousel',

            'core/CoreModule',
            'landing/LandingModule'
        ],
        function (require, angular)
        {
            var appName = 'blackdataLanding',
                dependencies = [
                    'ui.bootstrap',
                    'ngAnimate',
                    'ngMessages',
                    'ngSanitize',
                    'ui.router',
                    'pascalprecht.translate',
                    'slick',

                    'LocalStorageModule',
                    'blackdataLanding.CoreModule',
                    'blackdataLanding.LandingModule'
                ],
                app = angular
                    .module(appName, dependencies)
                    .config( ['$translateProvider', function ( $translateProvider )
                    {
                        $translateProvider.useStaticFilesLoader({
                            files: [
                                {
                                    prefix: './public/build/dist/assets/data/i18n/common/',
                                    suffix: '.json'
                                },
                                {
                                    prefix: './public/build/dist/assets/data/i18n/landing/',
                                    suffix: '.json'
                                }
                            ]
                        });
                        $translateProvider.preferredLanguage('en_us');
                    }])
                    .config(['localStorageServiceProvider', function (localStorageServiceProvider) {
                        localStorageServiceProvider
                            .setPrefix(appName)
                            .setStorageType('sessionStorage')
                            .setNotify(true, true);
                    }])
                    .config( ['$stateProvider', '$urlRouterProvider', '$locationProvider', function ( $stateProvider, $urlRouterProvider, $locationProvider )
                    {
                        var routes = [
                            {
                                state: 'landing',
                                options: {
                                    url: '/{dummyParam:[/]?}',
                                    views: {
                                        'container@': {
                                            templateUrl: './public/build/dist/assets/views/landing/landing.tpl.html',
                                            controller: 'LandingController'
                                        },
                                        'captcha@landing': {
                                            templateUrl: './public/build/dist/assets/views/landing/captcha.tpl.html',
                                            controller: 'CaptchaController'
                                        },
                                        'header@landing': {
                                            templateUrl: './public/build/dist/assets/views/landing/topBar.tpl.html'
                                        },
                                        'footer@landing': {
                                            templateUrl: '/public/build/dist/assets/views/landing/bottomBar.tpl.html'
                                        },
                                        'section1@landing': {
                                            templateUrl: './public/build/dist/assets/views/landing/partials/section1.tpl.html'
                                        },
                                        'section2@landing': {
                                            templateUrl: './public/build/dist/assets/views/landing/partials/section2.tpl.html'
                                        },
                                        'section3@landing': {
                                            templateUrl: './public/build/dist/assets/views/landing/partials/section3.tpl.html'
                                        },
                                        'areas-carousel@landing': {
                                            templateUrl: './public/build/dist/assets/views/landing/partials/areas-carousel.tpl.html'
                                        }
                                    }
                                }
                            }
                        ];

                        for(var i = 0; i < routes.length; i++) {

                            routes[i].options.params = {
                                humanFb: null,
                                apiFb: null
                            };
                            $stateProvider.state(routes[i].state, routes[i].options);
                        }

                        $locationProvider.html5Mode(true);
                    }] )
                    .run(['sessionConstantsService', '$rootScope', '$timeout', '$translate', '$filter', function (sessionConstantsService, $rootScope, $timeout, $translate, $filter) {

                        function initLanguagePreferences() {
                            var lang = sessionConstantsService.getLanguage();
                            lang = (lang)?lang:'en_us';

                            $rootScope.languagePreference = {currentLanguage: lang};

                            setLanguageByKey(lang);

                            $rootScope.languagePreference.switchLanguage = function (key, doBroadcast) {
                                setLanguageByKey(key);
                                if (doBroadcast)$rootScope.$broadcast('CHANGE_LANGUAGE', key);
                            };

                            $rootScope.languagePreference.translateKey = function (key) {
                                return $filter('translate')(key);
                            };
                        }

                        function setLanguageByKey(key) {

                            sessionConstantsService.setLanguage(key);
                            $translate.use(key);
                            $rootScope.languagePreference.currentLanguage = key;
                        }

                        initLanguagePreferences();
                    }]);


            //We manually start this bootstrap process; since ng:app is gone
            //( necessary to allow Loader splash pre-AngularJS activity to finish properly )
            require(['domReady!'], function (document) {
                //angular.bootstrap( document.getElementsByTagName('body')[0], [ appName ]);
                angular.bootstrap( document, [ appName ]);
            });
            return app;
        }
    );
}(define));
