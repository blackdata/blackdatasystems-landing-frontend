(function (define) {
    'use strict';

    define([
            'angular',
            'landing/LandingController',
            'landing/LandingService',
            'landing/FocusDirective'
        ],
        /**
         * 
         * @param angular
         * @param LandingController
         * @param LandingService
         * @param FocusDirective
         * @returns {string}
         */
        function ( angular, LandingController, LandingService, FocusDirective )
        {
            var moduleName = 'blackdataLanding.LandingModule';

            angular
                .module(moduleName, [])
                .service( 'focusService', FocusDirective)
                .controller('LandingController', LandingController)
                .service('landingService', LandingService);

            return moduleName;
        });

}(define));