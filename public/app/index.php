<!DOCTYPE html>
<html xmlns-ng="http://angularjs.org/" >
    <head>
        <base href="/">
        <meta http-equiv="content-type" content="text/html; charset=UTF8"/>
        <meta name="robots" content="index, follow">
        <meta name="geo.region" content="ES-CT" />
        <meta name="geo.placename" content="Barcelona" />
        <meta name="geo.position" content="41.399828;2.177528" />
        <meta name="ICBM" content="41.399828, 2.177528" />
        <meta name="description" content="Empresa especialista en seguridad y administración de sistemas: Arquitectura y consultoría Cloud, Elastic Cloud y Software. Soluciones IT personalizadas."/>
        <meta name="keywords" content="TODO">
        <meta name="author" content="BlackData S.L.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"Organization","url":"http:\/\/www.blackdata.systems\/“,”sameAs":[],"name”:”BlackData Systems”,”logo":"http:\/\/www.blackdata.systems\/public\/build\/dist\/assets\/images\/blackdata-logo.png"}</script>

        <title>BlackData Systems</title>

        <link rel="icon" href="./public/build/dist/assets/images/favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="./public/build/bower_components/bootstrap/dist/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="./public/build/dist/assets/css/normalize.css"/>
        <link rel="stylesheet" type="text/css" href="./public/build/dist/assets/libs/css/uiMorph/component.css" />
        <link rel="stylesheet" type="text/css" href="./public/build/dist/assets/libs/css/uiMorph/content.css" />
        <link rel="stylesheet" type="text/css" href="./public/build/dist/assets/css/landing/main.css"/>
        <link rel="stylesheet" type="text/css" href="./public/build/dist/assets/css/captcha.css"/>
    </head>
    <style>body {background: #000000;}</style>
    <body>
        <noscript><div><div class="logo"></div><p>Your browser has blocked Javascript. Please, enable Javascript to view the page content.</p></div></noscript>

        <div id='main-container' ui-view="container" class="appear"></div>

        <script src="./public/build/bower_components/requirejs/require.js"></script>
        <script>
            // obtain requirejs config
            require([
                'require',
                './public/build/dist/src/config-require'
            ], function (require, config) {
                // update global require config
                window.require.config(config);
                // load app
                require(['./public/build/dist/src/landing/main.js']);
            });
        </script>
    </body>
</html>