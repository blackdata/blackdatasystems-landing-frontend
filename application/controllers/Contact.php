<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/BaseController.php');

class Contact extends BaseController
{
    const SESSION_TRIES = 15;

    /**
     * @var array
     */
    protected $rate_limits = array('limit' => 180, 'window' => 900);

    public function __construct()
    {
        parent::__construct();
        $this->load->model('contact_model');
        $this->loadSession();
        $this->checkSessionTries();
    }

    /**
     * Creates a new contact.
     *
     * @return string
     */
    public function index_post()
    {
        try {
            // Checks rate limit.
            $this->checkRateLimit('user_post');

            // Extracts post parameters.
            $email       = $this->post('email');
            $name        = $this->post('name');
            $company     = $this->post('company');
            $position    = $this->post('position');
            $message     = $this->post('message');

            $response = $this->executePost($email, $name, $company, $position, $message);

            $this->initializeMailConfig();

            // Send mail to us.
            $mailData = array();
            $mailData['email']      = $email;
            $mailData['name']       = $name;
            $mailData['company']    = $company;
            $mailData['position']   = $position;
            $mailData['message']    = $message;
            $this->sendMailToBlackData($name, $mailData);

            // Send mail to the user.
            $mailData = array();
            $mailData['name']       = $name;
            $this->sendMailToClient($email, $mailData);

            $this->response($response, REST_Controller::HTTP_OK);
        } catch (\Exception $e) {
            $this->throwError('000');
        }
    }

    protected function initializeMailConfig()
    {
        $config = array();
        $config['protocol']     = 'smtp';
        $config['useragent']    = 'linux';
        $config['smtp_host']    = 'smtp.blackdata.systems';
        $config['smtp_user']    = 'no-reply@blackdata.systems';
        $config['smtp_pass']    = 'QfWnX:b4+yiA';
        $config['smtp_port']    = 25;
        $config['smtp_timeout'] = 5;
        $config['mailtype']     = 'html';
        $config['charset']      = 'iso-8859-1';
        $config['wordwrap']     = true;

        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");

    }

    protected function sendMailToBlackData($contactName, $messageData)
    {
        $this->email->from('no-reply@blackdata.systems', 'BlackData Systems');
        $this->email->to('contact@blackdata.systems');
//        $this->email->cc('another@another-example.com');

        $this->email->subject(ucfirst($contactName) . ' wants to contact with you');

        $mailBody = $this->load->view('templates/mail_blackdata.php', $messageData, true);
        $this->email->message($mailBody);

        $this->sendMail();
    }

    protected function sendMailToClient($receiver, $messageData)
    {
        $this->email->from('no-reply@blackdata.systems', 'BlackData Systems');
        $this->email->to($receiver);

        $this->email->subject('Your request has been sent');

        $mailBody = $this->load->view('templates/mail_client.php', $messageData, true);
        $this->email->message($mailBody);

        $this->sendMail();
    }

    protected function sendMail()
    {
        if (!$this->email->send()) {
            $this->throwError('006');
        }
    }

    public function executePost($email, $name, $company, $position, $message)
    {
        list($email, $name, $company, $position, $message) = $this->validate($email, $name, $company, $position, $message);

        $this->contact_model->set_contact_information($email, $name, $company, $position, $message);

        // Returns email.
        return array('email' => $email);
    }

    /**
     * Validates form inputs.
     *
     * @param string $email
     * @param string $name
     * @param string $company
     * @param string $message
     * @return array
     */
    protected function validate($email, $name, $company, $position, $message)
    {
        $email      = $this->validateEmail('email', $email);
        $name       = $this->validateString('name', $name, 1, 64);
        $company    = $this->validateString('company', $company, 1, 256);
        $message    = $this->validateString('message', $message, 1, 2048);

        if (isset($position)) {
            $position = $this->validateString('position', $position, 0, 256);
        }

        return array($email, $name, $company, $position, $message);
    }

    protected function checkSessionTries()
    {
        $currentTries = $this->sessionModel->getContactTries();
        $currentTries += 1;

        if ($currentTries >= $this::SESSION_TRIES) {
            $this->throwError('005');
        } else {
            $this->sessionModel->setContactTries($currentTries);
            $this->sessionModel->update();
        }
    }
}
