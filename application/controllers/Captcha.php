<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . 'libraries/BaseController.php');
require(APPPATH . 'libraries/CaptchaUtils.php');

class Captcha extends BaseController
{
    /**
     * Session constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('session_model');
        $this->config->load('captcha');
        $this->loadSession();
    }

    /**
     * Main function for the GET method.
     */
    public function index_get()
    {
        try {
            $response = $this->executeGet();

            $this->response($response, REST_Controller::HTTP_OK);
        } catch (\Exception $e) {
            $this->throwError('000');
        }
    }

    /**
     * Main function for the POST method.
     */
    public function index_post()
    {
        try {
            $solution  = $this->post('solution');

            $response = $this->executePost($solution);

            $this->response($response, REST_Controller::HTTP_OK);
        } catch (\Exception $e) {
            $this->throwError('000');
        }
    }

    /**
     * Logic of the GET method.
     *
     * @return array
     */
    public function executeGet()
    {
        $settings = $this->config->item('captcha');

        $captcha = new CaptchaUtils();
        $captcha->allowedWebCharacters    = $settings['allowed_web'];
        $captcha->allowedMobileCharacters = $settings['allowed_mobile'];
        $captcha->fontSketch              = $settings['font_sketch'];
        $captcha->fontSketchLowerLimit    = $settings['font_sketch_lower_limit'];
        $captcha->fontSketchUpperLimit    = $settings['font_sketch_upper_limit'];
        $captcha->fontAngleLowerLimit     = $settings['font_angle_lower_limit'];
        $captcha->fontAngleUpperLimit     = $settings['font_angle_upper_limit'];
        $captcha->fontHeightVariance      = $settings['font_height_variance'];
        $captcha->fontSizeUpperLimit      = $settings['font_size_upper_limit'];
        $captcha->fontSizeLowerLimit      = $settings['font_size_lower_limit'];
        $captcha->fontFile                = STATIC_FONT_DIR . $settings['font_file'];
        $captcha->lineNumberLowerLimit    = $settings['line_number_lower_limit'];
        $captcha->lineNumberUpperLimit    = $settings['line_number_upper_limit'];
        $captcha->lineLengthVariance      = $settings['line_length_variance'];
        $captcha->imageLength             = $settings['image_length'];
        $captcha->imageWidth              = $settings['image_width'];
        $captcha->imageHeight             = $settings['image_height'];
        $captcha->imageBgColor            = $settings['image_bg_color'];
        $captcha->imageFgColor            = $settings['image_fg_color'];
        $captcha->imageSwirlLowerLimit    = $settings['image_swirl_lower_limit'];
        $captcha->imageSwirlUpperLimit    = $settings['image_swirl_upper_limit'];
        $captcha->imageFileFormat         = $settings['image_file_format'];
        $captcha->compressionQuality      = $settings['compression_quality'];

        // Generates the captcha text and the image.
        $captchaSolution = $captcha->generateRandomString(true);
        $rawCaptcha      = $captcha->generateCaptcha($captchaSolution);

        $this->sessionModel->setCaptchaSolution($captchaSolution);
        $this->sessionModel->update();

        // Returns the captcha as a byte image (not base64).
        return array(
            'solution' => base64_encode($rawCaptcha)
        );
    }

    /**
     * Logic of the POST method.
     *
     * @param string $solution
     * @return array
     */
    public function executePost($solution)
    {
        $this->validateString('solution', $solution, 6, 6);

        $captchaSolution = $this->sessionModel->getCaptchaSolution();
        if (!isset($captchaSolution)) {
            $this->throwError('004');
        }

        // Removes captcha solution from the session and save it.
        $this->sessionModel->setCaptchaSolution(null);

        // Checks if the user solution is equal to the server one.
        if ($captchaSolution === null || $solution !== $captchaSolution) {
            $this->sessionModel->update();
            $this->throwError('004');
        } else {
            $this->sessionModel->setContactTries(0);
            $this->sessionModel->update();
        }
    }
}
