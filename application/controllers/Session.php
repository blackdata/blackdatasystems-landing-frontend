<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . 'libraries/BaseController.php');
require(APPPATH . 'libraries/SecurityUtils.php');

class Session extends BaseController
{
    const SESSION_ID_LENGTH          = 64;
    const SESSION_ZEROS              = 2;
    const NONCE_LENGTH               = 32;
    const SESSION_CHALLENGE_INTERVAL = 30;
    const SESSION_TIMEOUT            = 1800;
    const SIGNATURE_HASH_ALGORITHM   = 'sha256';
    const CHALLENGE_HASH_ALGORITHM   = 'sha256';
    const SECRET_KEY                 = 'orXn538gLa682hwENavBZMF9jjQWCLLkqQ5fMkDKhRPcCS53fnSQzlr5DR6m6QBM';

    /**
     * @var SecurityUtils
     */
    protected $utils;

    /**
     * Session constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->utils = new SecurityUtils();
        $this->load->model('session_model');
    }

    /**
     * Main function for the GET method.
     */
    public function index_get()
    {
        try {
            $response = $this->executeGet();

            $this->response($response, REST_Controller::HTTP_OK);
        } catch (\Exception $e) {
            $this->throwError('000');
        }
    }

    /**
     * Main function for the POST mehtod.
     */
    public function index_post()
    {
        try {
            $nonce     = $this->post('nonce');
            $signature = $this->post('signature');
            $solution  = $this->post('solution');
            $timestamp = $this->post('timestamp');
            $zeros     = $this->post('zeros');

            $response = $this->executePost($nonce, $signature, $solution, $timestamp, $zeros);

            $this->response($response, REST_Controller::HTTP_OK);
        } catch (\Exception $e) {
            $this->throwError('000');
        }
    }

    /**
     * Logic of the GET method.
     *
     * @return array
     */
    public function executeGet()
    {
        // Get the current time and number of initial zeros.
        $timestamp = time();
        $nonce     = $this->utils->generateRandomBytes($this::NONCE_LENGTH);

        // Compute the challenge signature and generate the structure.
        $signature = $this->generateSignature($timestamp, $nonce, $this::SESSION_ZEROS);

        return array(
            'nonce'     => base64_encode($nonce),
            'timestamp' => $timestamp,
            'zeros'     => $this::SESSION_ZEROS,
            'signature' => $signature,
        );
    }

    /**
     * Logic of the POST method.
     *
     * @param string $nonce
     * @param string $signature
     * @param string $solution
     * @param int $timestamp
     * @param int $zeros
     * @return array
     */
    public function executePost($nonce, $signature, $solution, $timestamp, $zeros)
    {
        // Validate the input.
        list($nonce, $signature, $solution, $timestamp, $zeros) = $this->validate($nonce, $signature, $solution, $timestamp, $zeros);


        // Ensure that the timestamp, signature and challenge are correct.
        $this->checkTimestamp($timestamp);
        $this->checkSignature($timestamp, $nonce, $signature, $zeros);
        $this->checkChallenge($solution, $signature, $zeros);

        // Save the session to the database.
        $sessionId = $this->createSessionId($this::SESSION_ID_LENGTH);

        try {
            $this->session_model->setId($sessionId);
            $this->session_model->setExpiresAt(date('Y-m-d G:i:s', time() + $this::SESSION_TIMEOUT));
            $this->session_model->setCaptchaSolution(null);
            $this->session_model->setContactTries(0);
            $this->session_model->setCreatedAt(date('Y-m-d G:i:s'));
            $this->session_model->insert();
        } catch (\Exception $e) {}

        // Returns session challenge.
        return array(
            'session' => $sessionId
        );
    }

    /**
     * Generates the session ID. The characters of the session ID are Base64.
     *
     * @param int $length
     * @return string
     */
    public function createSessionId($length)
    {
        return $this->utils->generateRandomStringBase64($length);
    }

    /**
     * Validates the parameters of the POST.
     *
     * @param string $nonce
     * @param string $signature
     * @param string $solution
     * @param int $timestamp
     * @param int $zeros
     * @return array
     */
    protected function validate($nonce, $signature, $solution, $timestamp, $zeros)
    {
        $nonce       = $this->validateString('nonce',     $nonce, 1, 64);
        $company     = $this->validateString('signature', $signature, 1, 256);
        $solution    = $this->validateString('solution',  $solution, 1, 64);
        $timestamp   = $this->validateInt('$timestamp',   $timestamp, 1, 2000000000);
        $zeros       = $this->validateInt('$zeros',       $zeros, 0, 16);

        return array($nonce, $company, $solution, $timestamp, $zeros);
    }

    /**
     * Given the structure timestamp, checks that is correct. The user only has
     * a defined interval to solve the challenge. So, if the timestamp in the
     * structure plus the allowed interval is greater than the current time,
     * throw an exception.
     *
     * @param int $timestamp
     */
    protected function checkTimestamp($timestamp)
    {
        // Check that the timestamp is withing the allowed interval.
        $computedInterval = time() - $timestamp;

        if (0 > $computedInterval || $computedInterval > $this::SESSION_CHALLENGE_INTERVAL) {
            $this->throwError('003');
        }
    }

    /**
     * Checks that the signature is correct. Given the leading zeros,
     * timestamp and nonce, it creates the structure and then signs it.
     * If the given signature is not equal to the computed one, throws an
     * exception.
     *
     * @param int $timestamp
     * @param string $nonce
     * @param string $signature
     * @param int $zeros
     */
    protected function checkSignature($timestamp, $nonce, $signature, $zeros)
    {
        // Generate the signature.
        $challengeSignature = $this->generateSignature($timestamp, base64_decode($nonce), $zeros);

        // Compare the computed signature with the one that is in the HTTP body.
        if (!$this->compare($challengeSignature, $signature)) {
            $this->throwError('003');
        }
    }

    /**
     * Checks that the challenge has been solved correctly. If not, throw an
     * exception. Steps done:
     *    (1) Concatenate the signature and the solution, and then hash it
     *        using the Sha1 algorithm.
     *    (2) Convert the hash solution to hexadecimal. If the hexadecimal
     *        solution starts with $zeros, the challenge is correct.
     *
     * @param string $solution
     * @param string $signature
     * @param int $zeros
     */
    protected function checkChallenge($solution, $signature, $zeros)
    {
        // Concatenate the signature and the solution fields from the HTTP body
        // and compute the Sha1 hash.
        $computedHash = bin2hex($this->utils->hash($signature . $solution, $this::CHALLENGE_HASH_ALGORITHM));

        // Check if the computed hash starts with the number of zeros set by
        // '$zeros'. If not valid, return false and the error message.
        // Otherwise return true.
        if (str_repeat('0', $zeros) !== substr($computedHash, 0, $zeros)) {
            $this->throwError('003');
        }
    }

    /**
     * Securely compares two strings so as to avoid timing attacks.
     *
     * @param string $string1
     * @param string $string2
     * @return bool
     */
    public function compare($string1, $string2)
    {
        if (!is_string($string1) || !is_string($string2)) {
            return false;
        }

        $len = strlen($string1);
        if ($len !== strlen($string2)) {
            return false;
        }

        $status = 0;
        for ($i = 0; $i < $len; $i++) {
            $status |= ord($string1[$i]) ^ ord($string2[$i]);
        }
        return $status === 0;
    }

    /**
     * Generates the signature of the challenge.
     *
     * @param int $timestamp
     * @param string $nonce
     * @param int $zeros
     * @return string
     */
    protected function generateSignature($timestamp, $nonce, $zeros)
    {
        return $this->utils->signatureHashBase64(
            $nonce . $timestamp . $zeros,
            $this::SECRET_KEY,
            $this::SIGNATURE_HASH_ALGORITHM,
            $nonce
        );
    }
}
