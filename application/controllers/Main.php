<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/BaseController.php');

class Main extends CI_Controller
{
    /**
     * Contact constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->database();
    }

    public function index()
    {
        $this->load->view('../../public/build/dist/index');
    }
}