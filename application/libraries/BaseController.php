<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require('REST_Controller.php');

class BaseController extends REST_Controller
{
    /**
     * @var Session_model
     */
    protected $sessionModel;

    /**
     * Contact constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->config->load('errors');
    }

    public function throwError($errorCode, $context = array(), $fieldName = '')
    {
        $errorMessages = $this->config->item('errors');

        if (!isset($errorMessages[$errorCode])) {
            $errorCode = $this->config->item('default_error');
        }
        $error = $errorMessages[$errorCode];

        $response = array(
            'http_code'  => $error['http_code'],
            'error_code' => $errorCode,
            'message'    => $this->createErrorMessage($error['message'], $context)
        );

        if(isset($fieldName) && $fieldName !== '') {
            $response['field'] = $fieldName;
        }

        $this->response($response, $error['http_code']);
    }

    private function createErrorMessage($message, $context)
    {
        // Remove the ending blank spaces and if the message doesn't end with
        // a dot, add it.
        $message = rtrim($message);
        if (strlen($message) > 0 && substr($message, -1) !== '.') {
            $message = $message . '.';
        }

        // For each item in the context, do:
        //    1) If its value is null, translate it to "NULL".
        //    2) If its value is a boolean, translate it to "TRUE" or "FALSE".
        //    3) If the value must be delimited between quotes, delimit it.
        //    4) Replace the variables by the ones in the context.
        foreach ($context as $key => $value) {
            if (!isset($value)) {
                $value = 'NULL';
            }
            if (is_bool($value)) {
                $value = ($value ? 'TRUE' : 'FALSE');
            }

            $message = str_replace('{{' . $key . '}}', $value , $message);
        }

        return $message;
    }

    /**
     * @param string $fieldName
     * @param string $fieldValue
     * @param null|int $minLength
     * @param null|int $maxLength
     * @return string
     */
    public function validateString($fieldName, $fieldValue, $minLength = null, $maxLength = null)
    {
        if (!isset($fieldValue)) {
            $this->throwError('002', array(
                'field_name' => $fieldName
            ), $fieldName);
        }

        if (!is_string($fieldValue)) {
            $this->throwError('002', array(
                'field_name' => $fieldName
            ), $fieldName);
        }

        $trimmedFieldValue = trim($fieldValue);

        if (isset($minLength) && strlen($trimmedFieldValue) < $minLength) {
            $this->throwError('002', array(
                'field_name' => $fieldName
            ), $fieldName);
        }

        if (isset($maxLength) && strlen($trimmedFieldValue) > $maxLength) {
            $this->throwError('002', array(
                'field_name' => $fieldName
            ), $fieldName);
        }

        return $trimmedFieldValue;
    }

    /**
     * @param string $fieldName
     * @param string $fieldValue
     * @param null|int $min
     * @param null|int $max
     * @return int|string
     */
    public function validateInt($fieldName, $fieldValue, $min = null, $max = null)
    {
        if (!isset($fieldValue)) {
            $this->throwError('002', array(
                'field_name' => $fieldName
            ), $fieldName);
        }

        if (is_string($fieldValue)) {
            $fieldValue = trim($fieldValue);
        }

        if (!is_int($fieldValue)) {
            try {
                $fieldValue = intval($fieldValue);
            } catch (\Exception $e) {
                $this->throwError('002', array(
                    'field_name' => $fieldName
                ), $fieldName);
            }
        }

        if (isset($min) && strlen($fieldValue) <= $min) {
            $this->throwError('002', array(
                'field_name' => $fieldName
            ), $fieldName);
        }

        if (isset($max) && strlen($fieldValue) >= $max) {
            $this->throwError('002', array(
                'field_name' => $fieldName
            ), $fieldName);
        }

        return $fieldValue;
    }

    /**
     * @param string $fieldName
     * @param string $fieldValue
     * @return string
     */
    public function validateEmail($fieldName, $fieldValue)
    {
        $email = $this->validateString($fieldName, $fieldValue, 5, 256);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->throwError('002', array(
                'field_name' => $fieldName
            ));
        }

        return $email;
    }

    /**
     * @return string
     */
    public function getSessionFromHttpHeader()
    {
        $sessionId = $this->input->get_request_header('authorization', true);
        if (!isset($sessionId) || !is_string($sessionId) || empty($sessionId)) {
            $this->throwError('001');
        }

        return $sessionId;
    }

    /**
     * Loads session from database.
     * @return string
     */
    public function loadSession()
    {
        $sessionId = $this->getSessionFromHttpHeader();
        $this->load->model('session_model');

        $this->sessionModel = $this->session_model->get_session($sessionId);
        if (!isset($this->sessionModel)) {
            $this->throwError('003');
        }
    }

    /**
     * Checks rate limit.
     *
     * @param string $request
     */
    protected function checkRateLimit($request)
    {
        // Loads rate limit model.
        $this->load->model('rate_limit_model');

        // Gets rate limit limit and window.
        $limit  = $this->rate_limit_model->get_rate_limit_limit($request)[0]->limit;
        $window = $this->rate_limit_model->get_rate_limit_window($request)[0]->window;

        // Gets requests limit and window.
        $requestLimit  = $this->rate_limits['limit'];
        $requestWindow = $this->rate_limits['window'];

        // Gets actual minutes time.
        $minute = date('i') * 60;

        // Checks window.
        if(time() < $window){

            // Rate limit exceeded
            $this->response(array(
                'reason' => 'rate limit exceeded'
            ), REST_Controller::HTTP_TOO_MANY_REQUESTS);
        }else{
            // Updates rate limit window.
            $actualWindowSecond = floor($minute/$requestWindow) * $requestWindow;
            if($actualWindowSecond >= 3600){$actualWindowSecond = 0;}
            $actualWindowMinute = $actualWindowSecond/60;

            $dateTime = new DateTime('now');
            $dateTime->setTime($dateTime->format('G'), $actualWindowMinute, 0);
            $this->rate_limit_model->update_rate_limit_limit($request, 0);
            $this->rate_limit_model->update_rate_limit_window($request, $dateTime->getTimestamp());
        }

        // Updates rate limit.
        $this->rate_limit_model->update_rate_limit_limit($request, $limit + 1);

        // Checks limit.
        if($limit > $requestLimit){
            // Updates rate limit limit.
            $this->rate_limit_model->update_rate_limit_limit($request, 1);

            // Gets next window minute.
            $nextWindowSecond = floor($minute/$requestWindow) * $requestWindow + $requestWindow;
            if($nextWindowSecond >= 3600){$nextWindowSecond = 0;}
            $nextWindowMinute = $nextWindowSecond/60;

            // Updates rate limit window.
            $dateTime = new DateTime('now');
            $dateTime->setTime($dateTime->format('G'), $nextWindowMinute, 0);
            $this->rate_limit_model->update_rate_limit_window($request, $dateTime->getTimestamp());


            // Rate limit exceeded
            $this->response(array(
                'reason' => 'rate limit exceeded'
            ), REST_Controller::HTTP_TOO_MANY_REQUESTS);
        }
    }
}
