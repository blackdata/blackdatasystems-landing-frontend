<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CaptchaUtils
{
    private $_image;
    private $_canvas;
    public $allowedWebCharacters;
    public $allowedMobileCharacters;
    public $fontSketch;
    public $fontSketchLowerLimit;
    public $fontSketchUpperLimit;
    public $fontAngleLowerLimit;
    public $fontAngleUpperLimit;
    public $fontHeightVariance;
    public $fontSizeUpperLimit;
    public $fontSizeLowerLimit;
    public $fontFile;
    public $lineNumberLowerLimit;
    public $lineNumberUpperLimit;
    public $lineLengthVariance;
    public $imageLength;
    public $imageWidth;
    public $imageHeight;
    public $imageBgColor;
    public $imageFgColor;
    public $imageSwirlLowerLimit;
    public $imageSwirlUpperLimit;
    public $imageFileFormat;
    public $compressionQuality;

    public function __construct()
    {
        $this->_image = new \ImagickDraw();
        $this->_canvas = new \Imagick();
    }

    /**
     * Generates the captcha and returns it as a string in ASCII format.
     * Note that before calling this function all class public variables must
     * be set or an error will be thrown.
     *
     * @param string $text
     *
     * @return string
     */
    public function generateCaptcha($text = null)
    {
        // Configure the colors of the image.
        $this->configureColors();

        // Write the characters.
        if (!isset($text)) {
            $randomString = $this->generateRandomString(true);
        } else {
            $randomString = $text;
        }
        $this->writeCharacters($randomString);

        // Draw some straight lines in the background.
        //$this->drawStraightLines();

        // Distort the image and return it.
        $this->distortImage();
        $this->compressImage();

        return $this->outputImage();
    }

    /**
     * Generate a random string with a determined length. Depending on $isWeb,
     * it will use on set of characters or another when randomizing.
     *
     * @param bool $isWeb
     *
     * @return string
     */
    public function generateRandomString($isWeb)
    {
        if ($isWeb) {
            $allowedCharacters = $this->allowedWebCharacters;
        } else {
            $allowedCharacters = $this->allowedMobileCharacters;
        }

        return (new SecurityUtils())->generateRandomStringBase64($this->imageLength, $allowedCharacters);
    }

    /**
     * Configure the foreground and background color and the font type.
     */
    protected function configureColors()
    {
        // Set the background color.
        $background = new \ImagickPixel();
        $background->setColor($this->imageBgColor);

        // Set the font color and type.
        $this->_image->setFont($this->fontFile);
        $this->_image->setFillColor($this->imageFgColor);

        // Apply the background to the canvas.
        $this->_canvas->newImage(
            $this->imageWidth,
            $this->imageHeight,
            $background
        );
    }

    protected function writeCharacters($captchaString)
    {

        // Compute the total length of all characters and the width, size and
        // sketch of each character.
        $averageFontSize = $this->fontSizeUpperLimit - $this->fontSizeLowerLimit;
        $totalFontWidth = 0;
        $arrayFontWidth = array();
        $arrayFontSize = array();
        $arrayFontSketch = array();
        for ($i = 0; $i < strlen($captchaString); $i++) {
            // Compute the font size. Because `mt_rand` only outputs integers,
            // we have to multiply by 100, compute the random percentage and
            // then divide by 100.
            $fontSizePercentage = (mt_rand($this->fontSizeLowerLimit * 100, $this->fontSizeUpperLimit * 100)) / 100;
            $fontSize = $fontSizePercentage * $this->imageHeight;
            $this->_image->setFontSize($fontSize);
            array_push($arrayFontSize, $fontSize);

            // Compute the per-character font sketch percentage. We suppose that
            // the total available width for sketching is 100, and each char
            // will be placed between the range of 0 and 100 in an ascending
            // order. For example, after the loop the output could be:
            //   -> [15, 30, 45, 60, 75, 90].
            array_push($arrayFontSketch, (mt_rand($this->fontSketchLowerLimit, $this->fontSketchUpperLimit) + $i * 100) / strlen($captchaString));

            // Compute the total and per-character font width.
            $fontWidth = $this->_canvas->queryFontMetrics($this->_image, $captchaString[$i])['textWidth'];
            array_push($arrayFontWidth, $fontWidth);
            $totalFontWidth += $fontWidth;
        }

        // Compute the usable width, which will be used to space the characters.
        $usableWidth = $this->imageWidth - $totalFontWidth;
        if ($usableWidth < 0) {
            throw new \Exception('unexpected exception');
        }

        // Set the starting offset for the Y and X axis.
        $baseOffsetX = ($usableWidth * (1 - $this->fontSketch)) / 2;
        $baseOffsetY = $this->imageHeight / 2 + ($averageFontSize * $this->imageHeight) / 2;
        $currentOffsetX = $baseOffsetX;

        for ($i = 0; $i < strlen($captchaString); $i++) {
            // Compute the rotation of the character and Y offset.
            $numericSign = mt_rand(0, 1) ? 1 : -1;
            $angle = mt_rand($this->fontAngleLowerLimit, $this->fontAngleUpperLimit) * $numericSign;
            $offsetY = $baseOffsetY + $baseOffsetY * $this->fontHeightVariance * $numericSign;

            // Add the letter to the canvas.
            $this->_canvas->annotateImage($this->_image, $currentOffsetX, $offsetY, $angle, $captchaString[$i]);

            // Compute an average font size.
            $this->_image->setFontSize($arrayFontSize[$i]);

            // Compute the X offset for the next letter.
            $baseOffsetX += $arrayFontWidth[$i];
            $currentOffsetX = $baseOffsetX + ($arrayFontSketch[$i] * ($usableWidth * $this->fontSketch)) / 100;
        }
    }

    /**
     * Draw a random number of straight lines with random length and position.
     */
    protected function drawStraightLines()
    {
        // Compute randomly the number of lines that will be drawn.
        $numLines = mt_rand(
            $this->lineNumberLowerLimit,
            $this->lineNumberUpperLimit
        );

        for ($i = 0; $i < $numLines; $i++) {
            // Apply the line length variance to the width and the height so as
            // to have lines with random and variable length.
            $width = ($this->imageWidth * $this->lineLengthVariance) / 2;
            $height = ($this->imageHeight * $this->lineLengthVariance) / 2;

            // Compute the source, destination and draw the line.
            $sourceX = mt_rand(-$width, $width);
            $sourceY = mt_rand(-$height, $height);
            $endX = mt_rand(-$width, $width);
            $endY = mt_rand(-$height, $height);
            $this->_image->line($sourceX, $sourceY, $endX, $endY);
        }
    }

    /**
     * Set randomly the swirl strength and then apply this effect on the whole
     * image.
     */
    protected function distortImage()
    {
        $swirlStrength = mt_rand(
            $this->imageSwirlLowerLimit,
            $this->imageSwirlUpperLimit
        );
        $this->_canvas->swirlImage($swirlStrength);
    }

    protected function compressImage()
    {
        if (is_int($this->compressionQuality)) {
            $this->_canvas->setImageCompression(true);
            $this->_canvas->setImageCompressionQuality($this->compressionQuality);
        }
    }

    /**
     * Save the image and render it on the specified file format.
     */
    protected function outputImage()
    {
        $this->_canvas->drawImage($this->_image);
        $this->_canvas->setImageFormat($this->imageFileFormat);

        return $this->_canvas->getImageBlob();
    }
}