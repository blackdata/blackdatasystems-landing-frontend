<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SecurityUtils
{
    const CHARSET_BASE64 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+/';

    /**
     * @param int $length
     * @param string|null $charset
     * @return string
     */
    function generateRandomStringBase64($length, $charset = null) {
        if (!isset($charset)) {
            $charset = $this::CHARSET_BASE64;
        }
        $charactersLength = strlen($charset);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $charset[mt_rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * @param int $length
     * @return string
     */
    function generateRandomBytes($length)
    {
        return openssl_random_pseudo_bytes($length);
    }

    /**
     * @param $string
     * @param string $salt
     * @param string $algorithm
     * @return string
     */
    public function hash($string, $algorithm, $salt = '')
    {
        return hash($algorithm, $salt . $string, true);
    }

    /**
     * @param string $string
     * @param string $key
     * @param string $salt
     * @param string $algorithm
     * @return string
     */
    public function signatureHash($string, $key, $algorithm, $salt = '')
    {
        return $this->hash($key . $string, $algorithm, $salt);
    }

    /**
     * @param $string
     * @param string $key
     * @param string $salt
     * @param string $algorithm
     * @return string
     */
    public function signatureHashBase64($string, $key, $algorithm, $salt = '')
    {
        return base64_encode($this->signatureHash($string, $key, $algorithm, $salt));
    }

    /**
     * @param $string
     * @param string $algorithm
     * @param string $salt
     * @return string
     */
    public function hashBase64($string, $algorithm, $salt = '')
    {
        return base64_encode($this->hash($string, $algorithm, $salt));
    }
}
