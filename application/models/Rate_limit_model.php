<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rate_limit_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Updates rate limit limit.
     *
     * @param string $request
     * @param int $rate_limit
     * @return string
     */
    public function update_rate_limit_limit($request, $rate_limit)
    {
        $data = array(
            'limit' => $rate_limit
        );

        $this->db->where('request', $request);
        $this->db->update('rate_limit', $data);
    }

    /**
     * Updates rate limit window.
     *
     * @param string $request
     * @param int $rate_window
     */
    public function update_rate_limit_window($request, $rate_window)
    {
        $data = array(
            'window' => $rate_window
        );

        $this->db->where('request', $request);
        $this->db->update('rate_limit', $data);
    }

    /**
     * Gets rate limit limit.
     *
     * @param string $request
     * @return int
     */
    public function get_rate_limit_limit($request)
    {
        $this->db->select('limit');
        $this->db->from('rate_limit');
        $this->db->where('request', $request);
        return $this->db->get()->result();
    }

    /**
     * Gets rate limit window.
     *
     * @param string $request
     * @return int
     */
    public function get_rate_limit_window($request)
    {
        $this->db->select('window');
        $this->db->from('rate_limit');
        $this->db->where('request', $request);
        return $this->db->get()->result();
    }
}