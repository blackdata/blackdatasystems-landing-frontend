<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Session_model extends CI_Model
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var int
     */
    public $expires_at;

    /**
     * @var string|null
     */
    public $captcha_solution;

    /**
     * @var int
     */
    public $contact_tries;

    /**
     * @var string
     */
    public $created_at;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Insert the session.
     */
    public function insert()
    {
        $this->removeExpiredSessions();
        $this->db->insert('session', $this);
    }

    /**
     * Update the session.
     */
    public function update()
    {
        $this->db->update('session', $this, array('id' => $this->id));
    }

    /**
     * Returns session.
     * @param string $id
     * @return $this
     */
    public function get_session ($id)
    {
        $this->db->from('session');
        $this->db->where('id', $id);

        $sessions = $this->db->get()->result();

        if(isset($sessions) && !empty($sessions)) {
            $result = $sessions[0];

            if (isset($result)) {
                $this->id = $result->id;
                $this->captcha_solution = $result->captcha_solution;
                $this->expires_at = $result->expires_at;
                $this->contact_tries = $result->contact_tries;
                $this->created_at = $result->created_at;

                $expiresAtTimestamp = strtotime($this->expires_at);
                if (time() >= $expiresAtTimestamp) {
                    $this->db->where('id', $id);
                    $this->db->delete('session');
                    return null;
                }

                return $this;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    protected function removeExpiredSessions()
    {
        $query = $this->db->query('SELECT id, expires_at FROM session WHERE expires_at < \'' . date('Y-m-d G:i:s') . '\'');
        $sessions = $query->result();

        if(isset($sessions) && is_array($sessions)) {
            foreach ($sessions as $session) {
                $expiresAtTimestamp = strtotime($session->expires_at);
                if (time() >= $expiresAtTimestamp) {
                    $this->db->where('id', $session->id);
                    $this->db->delete('session');
                }
            }
        }
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param int $expiresAt
     * @return $this
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expires_at = $expiresAt;

        return $this;
    }

    /**
     * @param string $captchaSolution
     * @return $this
     */
    public function setCaptchaSolution($captchaSolution)
    {
        $this->captcha_solution = $captchaSolution;

        return $this;
    }

    /**
     * @param int $contactTries
     * @return $this
     */
    public function setContactTries($contactTries)
    {
        $this->contact_tries = $contactTries;

        return $this;
    }

    /**
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getExpiresAt()
    {
        return $this->expires_at;
    }

    /**
     * @return null|string
     */
    public function getCaptchaSolution()
    {
        return $this->captcha_solution;
    }

    /**
     * @return int
     */
    public function getContactTries()
    {
        return $this->contact_tries;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
}