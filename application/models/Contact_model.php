<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Adds contact information to database.
     * @param $email
     * @param $name
     * @param $company
     * @param $position
     * @param $message
     */
    public function set_contact_information ($email, $name, $company, $position, $message)
    {
        $data = array(
            'email'       => $email,
            'name'        => $name,
            'company'     => $company,
            'position'    => $position,
            'message'     => $message,
            'created_at'  => date('Y-m-d G:i:s')
        );

        $this->db->insert('contact', $data);
    }
}