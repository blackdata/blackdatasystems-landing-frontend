<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['default_error'] = '000';
$config['errors'] = [
    // Default error. It can be thrown by any query.
    '000' => ['http_code' => 500, 'message' => 'unexpected error'],

    // The HTTP authorization header is missing. Session cannot be retrieved.
    '001' => ['http_code' => 401, 'message' => 'missing HTTP header field authorization'],

    // During a POST, one of the fields is not valid.
    '002' => ['http_code' => 400, 'message' => 'the following field is invalid: {{field_name}}'],

    // The obtained session is not valid.
    '003' => ['http_code' => 400, 'message' => 'invalid session'],

    // Invalid captcha solution.
    '004' => ['http_code' => 401, 'message' => 'invalid captcha solution'],

    // Max tries reached. Please, complete the captcha.
    '005' => ['http_code' => 401, 'message' => 'captcha required to complete the action'],
    
    // Cannot send the mail
    '006' => ['http_code' => 500, 'message' => 'cannot send the mail'],
];