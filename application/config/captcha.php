<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['captcha'] = array(
    // Allowed characters on web and mobiles.
    //'allowed_web' => 'abcdefghijkmnopstuvwyzABCDEFGHJKMNOPRSTUVWYZ',
    'allowed_web' => 'ABCDEFGHJKMNOPRSTUVWYZ',
    //'allowed_mobile' => '023456789',
    'allowed_mobile' => 'ABCDEFGHJKMNOPRSTUVWYZ',

    // Percentage range from 0 to 1 that specifies the variation of the
    // character size.
    'font_size_upper_limit' => 0.6,
    'font_size_lower_limit' => 0.3,

    // File that holds the font used.
    'font_file' => 'default.ttf',

    // Percentage from 0 to 1 from the overall 'white space' that is used
    // for sketching the characters. The left percentage is used as a
    // distance between the first and last character and the corner of the
    // image.
    'font_sketch' => 0.8,

    // Percentage from 0 to 100 that specifies the variation of the space
    // between each character.
    'font_sketch_lower_limit' => 25,
    'font_sketch_upper_limit' => 75,

    // Specifies the range in degrees of the possible variation of the
    // rotation of the character.
    'font_angle_lower_limit' => 10,
    'font_angle_upper_limit' => 25,

    // All characters are placed in the center of the Y axis. This
    // parameter specifies the percentage between 0 and 1 that a character
    // can vary its vertical position.
    'font_height_variance' => 0.1,

    // Minimum and maximum number of lines that can appear in the background
    // of the image.
    'line_number_lower_limit' => 6,
    'line_number_upper_limit' => 10,

    // Percentage of the variance of line length. It can range from 0 to N,
    // and if we suppose that the percentage is 'x', the formula is:
    //   -> ([(-width/2)*x, (width/2)*x], [(-height/2)*x, (height/2)*x])
    'line_length_variance' => 2,

    // Characters that will appear in each image.
    'image_length' => 6,

    // Width and height of the image.
    'image_width' => 250,
    'image_height' => 100,

    // Background and foreground colors of the image. You can set them by
    // name, such as 'blue', or by code like '#000000'. There is a special
    // color which is 'transparent'.
    'image_bg_color' => 'transparent',
    'image_fg_color' => '#648eda',

    // Percentage from 0 to 100 that represents the distortion that will
    // affect the overall image.
    'image_swirl_lower_limit' => 15,
    'image_swirl_upper_limit' => 35,

    // Format that will be used to render the image.
    'image_file_format' => 'png',
    'compression_quality' => 10
);