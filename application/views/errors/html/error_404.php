<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link rel="icon" href="./public/build/dist/assets/images/favicon.ico"/>
<title>BlackData S.L.</title>
<link rel="icon" href="./public/build/dist/assets/images/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="./public/build/dist/assets/css/landing/404.css"/>
</head>
<body class="appear">
	<div class="container">
		<div class="logo"></div>
		<div class="sub-header">
			<h1><?php echo $heading; ?></h1>
			<h2><?php echo $message; ?></h2>
		</div>
	</div>
</body>
</html>