<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Blackdata Systems</title>
    </head>

	<body style="margin: 0;padding: 0;font-family: sans-serif;background-color: #f0f0f0;text-align: justify;min-width: 100%!important;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 100%;max-width: 600px;">
                        <tr>
                            <td class="header" style="background-color: #363636;padding: 40px 30px 0 30px;">
                                <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="header-logo" height="80" style="padding: 0 0 20px 0;">
                                            <h1 class="text-logo align-center" style="color: #E6E6E6;font-weight: 100;font-size: 60px;line-height: 28px;text-align: center;">Blackdata Systems</h1>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="80" style="background: transparent;padding: 0 0 20px 0;">
                                            <h2 class="align-center text-logo-color" style="background: transparent;color: #E6E6E6;font-weight: 100;font-size: 22px;text-align: center;">Performance. Scalability. Security.</h2>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="content-body" style="background-color: #E0E0E0;padding: 30px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="100%">
                                            <div class="content-body-container" style="min-height: 300px;max-width: 650px;word-wrap: break-word;">
                                                <h1 style="background: none; color: #343434;font-weight: normal;font-size: 24px;line-height: 28px;">User <b><?php echo $name; ?></b> has contacted you!</h1>
                                                <h2 style="background: none; color: #343434;font-weight: normal;font-size: 18px;line-height: 20px;"><b>Email:</b> <?php echo $email; ?></h2>
                                                <h2 style="background: none; color: #343434;font-weight: normal;font-size: 18px;line-height: 20px;"><b>Company:</b> <?php echo $company; ?></h2>
                                                <h2 style="background: none; color: #343434;font-weight: normal;font-size: 18px;line-height: 20px;"><b>Position:</b> <?php echo $position; ?></h2>
                                                <h2 style="background: none; color: #343434;font-weight: normal;font-size: 18px;line-height: 20px;"><b>Message:</b></h2>
                                                <p style="font-size: 16px;"><?php echo $message; ?></p>
                                                <!--<a href="" class="btn" style="display: inline-block;padding: 12px 22px;background: none;border-radius: 4px;box-shadow: none;border: 2px solid #363636;text-decoration: none;vertical-align: middle;color: #363636;font-size: 12px;letter-spacing: 1px;font-weight: 700;line-height: 1.42857143;text-align: center;white-space: nowrap;text-transform: uppercase;cursor: pointer;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;">Outline</a>-->
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="footer" style="background-color: #363636;padding: 20px 30px 15px 30px;font-size: 12px;color: #ffffff;line-height: 16px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #363636;padding: 20px 30px 15px 30px;font-size: 12px;color: #ffffff;line-height: 16px;">
                                    <tr>
                                        <td>
                                            <p class="copyright align-center" style="text-align: center;font-size: 14px;">Copyright &reg;Blackdata Systems S.L. 2016.</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>