-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Temps de generació: 15-02-2016 a les 14:04:27
-- Versió del servidor: 5.5.47-0ubuntu0.14.04.1
-- Versió de PHP: 5.5.9-1ubuntu4.14

DROP DATABASE IF EXISTS `blackdata_landing`;
CREATE DATABASE `blackdata_landing`;
USE `blackdata_landing`;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de dades: `blackdata_landing`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `rate_limit`
--

CREATE TABLE IF NOT EXISTS `rate_limit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request` varchar(256) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `limit` int(11) NOT NULL,
  `window` int(11) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Bolcant dades de la taula `rate_limit`
--

INSERT INTO `rate_limit` (`request`, `limit`, `window`, `id`) VALUES
  ('user_post', 0, 1455541200, 1);

-- --------------------------------------------------------

--
-- Estructura de la taula `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) COLLATE latin1_bin NOT NULL,
  `name` varchar(256) COLLATE latin1_bin NOT NULL,
  `company` varchar(256) COLLATE latin1_bin DEFAULT NULL,
  `position` varchar(256) COLLATE latin1_bin DEFAULT NULL,
  `message` varchar(2000) COLLATE latin1_bin DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- --------------------------------------------------------

--
-- Estructura de la taula `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` char(64) COLLATE latin1_bin NOT NULL,
  `captcha_solution` char(6) COLLATE latin1_bin,
  `contact_tries` int(11) NOT NULL,
  `expires_at` TIMESTAMP NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
